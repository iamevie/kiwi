use kiwic::lexer::Lexer;

fn main() {
	let source = "abc \nvar \n\tbanane".as_bytes();
	let mut ast = Lexer::new(source).lex();
	ast.parse();

	dbg!(ast);
}
