use crate::tag::Tag;

#[derive(Default)]
pub struct TypeTable<'a> {
	names:		Vec<&'a str>,
	tags:		Vec<Tag>,
	starts:		Vec<usize>,
	ends:		Vec<usize>,
	parents:	Vec<Option<usize>>,
}

impl<'a> TypeTable<'a> {
	pub fn lookup_name(&self, name: &str) -> Result<usize, &str> {
		for (idx, found) in self.names.iter().enumerate() {
			if found == &name { return Ok(idx) }
		}
		
		Err("name is not in typetable")
	}

	pub fn push(&mut self, name: &'a str, tag: Tag, start: usize, end: usize, parent: Option<usize>) {
		self.names.push(name);
		self.tags.push(tag);
		self.starts.push(start);
		self.ends.push(end);
		self.parents.push(parent);
	}
}
