#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum Tag {
	Eos,

	Ident,

	Colon,
	Comma,
	Plus,
	Minus,
	Asterisk,
	Modulo,
	Slash,

	LParen,
	RParen,
	LSquirly,
	RSquirly,
	LBracket,
	RBracket,

	Function,
	Struct,
	Enum,
	Variable,
}
