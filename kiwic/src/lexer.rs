use crate::{ast::Ast, tag::Tag, span::Span};

pub struct Lexer<'a> {
	source:			&'a [u8],
	read:			usize,
	current:		u8,
	line_number:	usize,
	line_index:		usize,
}

impl<'a> Lexer<'a> {
	pub fn new(source: &'a [u8]) -> Self {
		let current = if source.is_empty() { b'\0' } else { source[0] };
		Self { source, read: 0, current, line_number: 1, line_index: 0, }
	}

	#[inline(always)]
	fn advance(&mut self) {
		self.read += 1;
		self.line_index += 1;
		self.current = self[self.read];
	}

	#[inline(always)]
	fn skip_whitespace(&mut self) {
		loop {
			match self.current {
				b'\n'	=> {
					self.advance();
					self.line_number += 1; self.line_index = 0
				},
				c if c.is_ascii_whitespace() => self.advance(),
				_ => break
			}
		}
	}

	#[inline(always)]
	fn peek(&self) -> u8 {
		self[self.read + 1]
	}

	pub fn lex(&mut self) -> Ast {
		let mut tags = Vec::new();
		let mut spans = Vec::new();
		let mut tag;
		let mut line_index;
		let mut read;
		loop {
			self.skip_whitespace();
			line_index = self.line_index;
			read = self.read;
			tag = match self.current {
				b':' 	=> Tag::Colon,
				b',' 	=> Tag::Comma,
				b'+' 	=> Tag::Plus,
				b'-' 	=> Tag::Minus,
				b'*' 	=> Tag::Asterisk,
				b'%' 	=> Tag::Modulo,
				b'/' 	=> Tag::Slash,

				b'(' 	=> Tag::LParen,
				b')' 	=> Tag::RParen,
				b'{' 	=> Tag::LSquirly,
				b'}' 	=> Tag::RSquirly,
				b'[' 	=> Tag::LBracket,
				b']' 	=> Tag::RBracket,

				b'\0'	=> break,

				x if x.is_ascii_alphabetic() || x == b'_' => {
					let start = self.read;
					while self.peek().is_ascii_alphabetic()
						|| self.peek().is_ascii_digit()
						|| self.peek() == b'_' { self.advance() }

					match &self.source[start..=self.read] {
						b"fun" 		=> Tag::Function,
						b"struct" 	=> Tag::Struct,
						b"enum" 	=> Tag::Enum,
						b"var" 		=> Tag::Variable,
						_ 			=> Tag::Ident
					}
				}
				_ => unimplemented!("Tag not found.")
			};

			self.advance();

			tags.push(tag);
			spans.push(Span::new(self.line_number, line_index, self.read - read));
		}
		let ast = Ast::new(tags, spans);
		ast
	}
}

impl std::ops::Index<usize> for Lexer<'_> {
	type Output = u8;

	fn index(&self, index: usize) -> &Self::Output {
		if self.source.len() <= index { &b'\0' }
		else { &self.source[index] }
	}
}

#[allow(unused)]
fn test_tags(source: &str, expected_tags: Vec<Tag>) {
	use std::iter::zip;

	let ast = Lexer::new(source.as_bytes()).lex();

	assert_eq!(expected_tags.len(), ast.tags.len());
	for (expected, found) in zip(expected_tags, ast.tags) {
		assert_eq!(expected, found);
	}
}

#[allow(unused)]
fn test_spans(source: &str, expected_spans: Vec<Span>) {
	use std::iter::zip;

	let ast = Lexer::new(source.as_bytes()).lex();

	assert_eq!(expected_spans.len(), ast.spans.len());
	for (expected, found) in zip(expected_spans, ast.spans) {
		assert_eq!(expected, found);
	}
}

#[test]
fn lex_spans() {
	test_spans("abc \nvar \n\tbanane", vec![
		Span::new(1, 0, 3), Span::new(2, 0, 3), Span::new(3, 1, 6)
	])
}

#[test]
fn lex_function() {
	test_tags("fn add : u8 : (a u8, b u8) { a + b }", vec![
		Tag::Function, Tag::Ident, Tag::Colon,
		Tag::Ident,	Tag::Colon,	Tag::LParen,
		Tag::Ident,	Tag::Ident,	Tag::Comma,
		Tag::Ident,	Tag::Ident,	Tag::RParen,
		Tag::LSquirly,	Tag::Ident,	Tag::Plus,
		Tag::Ident, Tag::RSquirly,
	]);

	test_tags("fn name :: {}", vec![
		Tag::Function, Tag::Ident, Tag::Colon,
		Tag::Colon, Tag::LSquirly, Tag::RSquirly
	]);
}
