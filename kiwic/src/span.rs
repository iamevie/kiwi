#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Span {
	line_number:	usize,
	line_index:		usize,
	length:			usize,
}

impl Span {
	pub fn new(line_number: usize, line_index: usize, length: usize) -> Self {
		Self { line_number, line_index, length }
	}
}
