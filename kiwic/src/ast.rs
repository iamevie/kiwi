use crate::{tag::Tag, span::Span, type_table::TypeTable};

#[derive(Debug)]
pub struct Ast {
	tags:		Vec<Tag>,
	spans:		Vec<Span>,
	current:	Tag,
}

impl Ast {
	pub fn new(tags: Vec<Tag>, spans: Vec<Span>) -> Self {
		Self { tags, spans }
	}

	pub fn parse(&mut self) -> TypeTable {
		let mut type_table = TypeTable::default();

		let (mut name, mut tag, mut start, mut end, mut parent);
		loop {
			match self.current {

				Tag::Function => {
					tag = self.current;
					(name, start, end, parent) = self.function()?;
				}

				Tag::Eos => break,

				_ => unimplemented!("Ast: This tag is not implemented")
			}
		}

		type_table
	}
}
